package testcases;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	static ExtentHtmlReporter report;
	static ExtentReports extent;
	@BeforeSuite
	public void startReport()
	{
	 report=new ExtentHtmlReporter("./reports/result1.html");
	 report.setAppendExisting(true);
	 extent=new ExtentReports();
     extent.attachReporter(report);
	}
	@Test
	public void reports() throws IOException
	{
		ExtentTest test=extent.createTest("cread lead", "cread a new lead");
		test.assignAuthor("gayatri");
		test.assignCategory("sanity");
		test.pass("test case passed");
		test.fail("testcase not passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snap1.png").build());
	}
@AfterSuite
public void run()
{
	extent.flush();
}
}
