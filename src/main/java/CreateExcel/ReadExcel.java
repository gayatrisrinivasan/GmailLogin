package CreateExcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import utils.Report;

public class ReadExcel extends Report {

	public static Object[][] fetchData() throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook book=new XSSFWorkbook("./Data/"+excelFileName+".xlsx");
		System.out.println(excelFileName);
		XSSFSheet sheetAt = book.getSheetAt(0);

		int rowNum = sheetAt.getLastRowNum();
		System.out.println("row count:"+rowNum);
		int cellNum = sheetAt.getRow(0).getLastCellNum();
		System.out.println("coloum count:"+cellNum);

		Object[][] data = new Object[rowNum][cellNum];

		for (int i = 1; i <= rowNum; i++) {
			XSSFRow row = sheetAt.getRow(i);
			for (int j = 0; j < cellNum; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);

				data[i-1][j] = stringCellValue;
			}

		}
		return data;
	}
}
