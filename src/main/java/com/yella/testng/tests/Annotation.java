package com.yella.testng.tests;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.yalla.selenium.api.base.SeleniumBase;

import CreateExcel.ReadExcel;

public class Annotation extends SeleniumBase{
	@DataProvider(name="CreateLead")
	public Object[][] fetchData() throws IOException {
		return ReadExcel.fetchData();
	}
	 @BeforeMethod(groups="any")
	  public void beforeMethod() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
	  }

	 @AfterMethod(groups="any")
	  public void afterMethod() {
		  close();
	  } 


}
