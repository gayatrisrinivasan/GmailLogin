package com.yella.testng.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class EditLead extends Annotation {
	@BeforeTest(groups="sanity")
	public void setData() {
		testcaseName= "TC002_EditLead";
		testcaseDec = "Edit the Lead in leaftaps";
		author      = "Gayatri";
		category    = "Smoke";
	}
	//@Test(dependsOnMethods={"com.yella.testng.tests.CreatLead.createLead"},timeOut=10000)
	@Test(groups="sanity",dependsOnGroups="smoke")
	public void editLead() throws InterruptedException {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath","//span[text()='Phone']"));
		clearAndType(locateElement("name", "phoneNumber"), "99"); 
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	}
}
