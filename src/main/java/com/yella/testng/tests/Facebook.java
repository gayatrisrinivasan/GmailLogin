package com.yella.testng.tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class Facebook extends SeleniumBase{
    @Test
	public void facebook() throws InterruptedException
	{
	 startApp("chrome","https://www.facebook.com/");
	 clearAndType(locateElement("xpath", "//input[@id='email']"),"8526463212" );
	 clearAndType(locateElement("id", "pass"),"saibaba97" );	
     click(locateElement("xpath", "//input[@value='Log In']"));
     Thread.sleep(4000);
     WebElement search = locateElement("class", "_1frb");
     clearAndType(search, "testleaf");
     Thread.sleep(4000);
     click(locateElement("xpath", "//button[@data-testid='facebar_search_button']"));
     verifyDisplayed(locateElement("xpath", "//div[text()='TestLeaf']"));
     WebElement like = locateElement("xpath", "(//button[@type='submit'])[2]");
     Thread.sleep(3000);
     if(like.getText().equalsIgnoreCase("Like"))
     {
    	 click(like);
    	 System.out.println("cliked");
     }
     else {
    	 System.out.println("it is already liked");
     }
     
     WebElement leaf = locateElement("xpath","//div[text()='TestLeaf']");
     click(leaf);
    
     partialtitle("(6) TestLeaf - Home");
     WebElement likes = locateElement("xpath","//div[contains(text(),'people like this')]");
     System.out.println(likes.getText());
     close();
     
}
}
