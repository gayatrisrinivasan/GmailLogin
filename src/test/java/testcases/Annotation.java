package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class Annotation extends SeleniumBase {
	@Parameters({"url","username","password"})
  @BeforeMethod() 
  public void beforeMethod(String url,String username,String password) 
  {
	  startApp("chrome", "url");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		 WebElement element = locateElement("link","CRM/SFA");
		 click(element);
		 WebElement element2 = locateElement("link","Leads");
		 click(element2);
	  
  }
  @AfterMethod
  public void afterMethod()
  {
	 close(); 
  }
}
